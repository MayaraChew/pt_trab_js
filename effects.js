// Deixando a cor dos valores positivos verde e a dos negativos vermelho

table = document.getElementById("minha-linda-tabela")

let total = 0

for (let i = 1; i < table.rows.length - 1; i++) {
    if (Number(table.rows[i].cells[3].innerText) < 0)  {
        table.rows[i].classList.add("table-danger")
    }
    if (Number(table.rows[i].cells[3].innerText) > 0) {
        table.rows[i].classList.add("table-success")
    }
    let valor = Number(table.rows[i].cells[3].innerText)
    total += valor
}
table.rows[table.rows.length - 1].cells[1].innerText = total.toFixed(2)

// Desafio - Dark e Light Mode

button = document.getElementById("toggle-dark-mode").children[0]   

button.onclick = function dark_mode() {
    button.className == "btn btn-dark" ? button.className = "btn btn-light" : button.className = "btn btn-dark"
    button.className == "btn btn-dark" ? button.innerText = "Toggle Dark-Arts Mode" : button.innerText = "Toggle Light-Arts Mode"
    
    for (i = 0; i < 4; i++) {
        table.rows[0].cells[i].className == "bg-light text-dark" ? table.rows[0].cells[i].className = "bg-dark text-light" : table.rows[0].cells[i].className = "bg-light text-dark"
    }

    body = document.getElementById("meu-container")
    body.parentElement.className == "bg-dark" ? body.parentElement.className = "bg-light" : body.parentElement.className = "bg-dark"

    total = document.getElementById("tabela-total")
    total = document.getElementById("tabela-total")
    total.cells[0].className == "text-dark" ? total.cells[0].className = "text-light" : total.cells[0].className = "text-dark"
    total.cells[1].className == "text-dark" ? total.cells[1].className = "text-light" : total.cells[1].className = "text-dark"

    nav = document.getElementById("meu-nav").children[0]
    nav.className == "text-dark" ? nav.className = "text-light" : nav.className = "text-dark"   
}